## RELEASE HISTORY

##### _Release date: 2021-11-25_

| Name   | Description |
| ------ | ------ |
| Application Name | Unitech_Reader_Demo_v1.5.7.2021112501.apk |
| Demo Version | v1.5.7.2021112501 |
| Supported F/W Version | ut-5.1.1.13.3 and above |

## What's New

 - [ ] 6B and Rail Tag Inventory Feature added
   -  by default 6C tag inventory is set.
     [ Settings - > Inventory Tag Type -> (6C/6B/Rail)]

 <details>
     <summary markdown="span" >Screenshots</summary>
      <div align="left">
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/tag_type_settings_01.png"  alt="Setting screen" title="Settings"</img>
        <b> >> </b>
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/tag_type_settings_02.png" alt="List screen" title="Setting Lists"></img>
        <b> >> </b>
        <img width="24%" src="https://gitlab.com/atid-share/asset/-/raw/main/tag_type_settings_03.png" alt="dialog " title="Option dialog"></img>
       </div>
      </details>


## Support
 For SDK and other inquiries please contact inquiry@atid1.com
